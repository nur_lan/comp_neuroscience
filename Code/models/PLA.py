from models.model import Model
from utils.utils import sign, visualize_classification
from settings import WEIGHTS_FILENAME, LOAD_WEIGHTS_FROM_FILE, BACKPROP_MAX_SAMPLES, SAVE_WEIGHTS_TO_FILE
from numpy import load as numpy_load
from numpy import save as numpy_save
from numpy import dot
from random import randint, shuffle


class PLAModel(Model):

    def __init__(self, data, params):
        Model.__init__(self, data, params)
        self.data = data
        self.params = params
        self.test_errors = []
        self.training_errors = []
        self.w = None
        self.error_train = None
        self.test_error = None
        self.accuracy = None

    def train_model(self):
        return self.train_pla()

    def train_pla(self):
        # load datasets
        train_samples = self.data['train_data']['x']
        train_targets = self.data['train_data']['y']
        test_samples = self.data['test_data']['x']
        test_targets = self.data['test_data']['y']

        num_of_features = len(train_samples[0])
        num_of_samples = len(train_samples)
        print("Number of samples:", min(num_of_samples, BACKPROP_MAX_SAMPLES))
        print("Number of features:", num_of_features)

        # load PLA parameters
        eta = self.params['eta']
        max_epoch = self.params['max_epoch']

        # algorithm variables
        if LOAD_WEIGHTS_FROM_FILE:
            weights_vector = numpy_load(WEIGHTS_FILENAME)
            print("Loaded saved weights from", WEIGHTS_FILENAME)
        else:
            weights_vector = [randint(0,2)-1 for i in range(num_of_features)]  # randomized weights vector with values (-1, 0, 1)
            # weights_vector = [0] * num_of_features

        # print("Initial weights vector:", weights_vector)

        sample_order = list(range(num_of_samples))
        shuffle(sample_order)  # randomize samples order

        errors_per_epoch = []
        for epoch in range(int(max_epoch)):
            errors_in_epoch = 0

            if epoch % 10 == 0:
                print("Epoch #{}".format(epoch))

            all_targets_satisfied = True     # reset convergence flag

            for s in sample_order[:BACKPROP_MAX_SAMPLES]:
                sample = train_samples[s]
                y_predicted = sign(dot(sample, weights_vector))  # predict target using current weights
                y_target = train_targets[s]

                if y_predicted != y_target:
                    all_targets_satisfied = False
                    errors_in_epoch += 1

                    for w, weight in enumerate(weights_vector):    # update weights according to error
                        weights_vector[w] += eta * (y_target-y_predicted) * sample[w]  # perceptron update rule: w(i) = w(i) + eta * (t-o) * x(i)
                        # print("updated weight {} to {}".format(w, weights_vector[w]))

            errors_per_epoch.append(errors_in_epoch)

            if all_targets_satisfied:   # all predictions were identical to targets, target converged. break epoch loop
                break

        print("Training errors:", errors_per_epoch)
        self.training_errors.append(errors_per_epoch)

        print("PLA training done. Used {} epochs". format(epoch+1))

        #save weights vector to disk
        if SAVE_WEIGHTS_TO_FILE:
            numpy_save(WEIGHTS_FILENAME, weights_vector)
            print('Saved weights to', WEIGHTS_FILENAME)

        self.w = weights_vector


    def evaluate_model(self, visualize=False):
        weights_vector = self.w
        classification_legend = {'-1': '8', '1': '5'}

        total_correct = 0
        total_incorrect = 0
        correct_exemplars = []
        incorrect_exemplars = []

        test_samples = self.data['test_data']['x']
        test_targets = self.data['test_data']['y']

        for s, sample in enumerate(test_samples):
            y_predicted = sign(dot(sample, weights_vector))
            if y_predicted == test_targets[s]:
                total_correct += 1
                if total_correct <= 15:
                    correct_exemplars.append([sample, classification_legend[str(y_predicted)]])
            else:
                total_incorrect += 1
                if total_incorrect <= 15:
                    incorrect_exemplars.append([sample, classification_legend[str(y_predicted)]])

        accuracy = total_correct / len(test_samples)
        test_error = total_incorrect / len(test_samples)

        print("Model evaluation:\n\t Accuracy: {}\tError: {}".format(str(accuracy), str(test_error)))

        self.test_error = test_error
        self.accuracy = accuracy

        if visualize:
            visualize_classification(correct_exemplars, incorrect_exemplars)