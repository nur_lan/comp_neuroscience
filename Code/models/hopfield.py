from models.model import Model
import numpy as np
from random import sample as random_sample
from random import shuffle
import matplotlib.pyplot as plt
from copy import deepcopy


class HopfieldModel(Model):

    def __init__(self, data, params):
        Model.__init__(self, data, params)
        self.data = data
        self.params = params
        self.num_of_units = params['num_of_units']
        self.weights = np.zeros((self.num_of_units, self.num_of_units))
        self.units = np.zeros(self.num_of_units)
        self.threshold = params['threshold']
        self.memories_train = self.data['train']
        self.memories_test = []
        self.results = {}
        self.statistics = {}

    def visualize_memory(self, memory):
        mat = self.memory_to_matrix(memory)
        plt.matshow(mat)
        plt.show()

    def show_all_training_set(self):
        for memory_name in self.memories_train:
            print("Showing training set memory:", memory_name)
            self.visualize_memory(self.memories_train[memory_name])

    def show_all_test_set(self):
        for memory_name in self.memories_test:
            print("Showing test set memory:", memory_name)
            self.visualize_memory(self.memories_test[memory_name])

    def train_model(self):
        for i in range(len(self.weights)):
            for j in range(i):
                sum = 0
                for memory in self.memories_train:
                    memory_array = self.memories_train[memory]
                    sum += memory_array[i] * memory_array[j]

                weight = sum / len(self.memories_train)
                self.weights[i][j] = weight
                self.weights[j][i] = weight
        # print("Finished training")

    def activate_unit(self, unit_index):
        sum = 0
        for j in range(len(self.units)):
            sum += self.weights[unit_index][j] * self.units[j]
        if sum > self.threshold:
            return 1
        return -1

    def feed_network(self, memory):
        USE_EPOCHS = True

        for i in range(len(memory)):
            self.units[i] = memory[i]
        if USE_EPOCHS:
            for epoch in range(self.params['epochs']):
                unit_index = np.random.random_integers(0, self.num_of_units-1)
                self.units[unit_index] = self.activate_unit(unit_index)
        else:
            index_order = list(range(self.num_of_units))
            shuffle(index_order)
            for unit_index in index_order:
                self.units[unit_index] = self.activate_unit(unit_index)

        return self.units

    def perturbate_target_memories(self):
        perturbated_memories = {}
        for memory_name in self.memories_train:
            memory = deepcopy(self.memories_train[memory_name])
            flip_indexes = random_sample(range(self.num_of_units), int(self.num_of_units*self.params['perturbation']))
            for flip_index in flip_indexes:
                memory[flip_index] = memory[flip_index] * -1
            perturbated_memories[memory_name] = memory

        return perturbated_memories

    def evaluate_model(self):
        VISUALIZE = False

        self.memories_test = self.perturbate_target_memories()

        recall_rates = []
        for memory_test_name in self.memories_test:
            memory_target = self.memories_train[memory_test_name]
            memory_test = self.memories_test[memory_test_name]
            memory_recalled = self.feed_network(memory_test)

            distance = self.memories_distance(memory_target, memory_recalled)
            recall_rates.append(distance)
            if VISUALIZE:
                print("Visualizing target", memory_test_name)
                self.visualize_memory(memory_target)
                print("Visualizing test", memory_test_name)
                self.visualize_memory(memory_test)
                print("Visualizing result", memory_test_name)
                self.visualize_memory(memory_recalled)

        num_of_stable_memories = 0
        for recall in recall_rates:
            if recall <= self.params['tolerance']:
                num_of_stable_memories += 1
        recall_rate = num_of_stable_memories / len(self.memories_test)
        self.results['recall_rate'] = recall_rate
        # print("Recall rate:", recall_rate)
        return recall_rate

    @staticmethod
    def memories_distance(mem1, mem2):
        flips = 0
        for i in range(len(mem1)):
            if mem1[i] != mem2[i]:
                    flips += 1
        return flips / (len(mem1))

    @staticmethod
    def flat_memory(memory):
        ret = []
        for row in memory:
            ret.extend(row)
        return ret

    @staticmethod
    def memory_to_matrix(memory):
        arr = np.array(memory)
        return arr.reshape((np.sqrt(len(memory)), np.sqrt(len(memory))))
