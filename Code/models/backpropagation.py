from models.model import Model
from utils.utils import sigmoid, linear_output, matrix_for_img_vector
from numpy import dot, array, mean
from random import randint, uniform
import matplotlib.pyplot as plt


class BackPropagationModel(Model):
    def __init__(self, data, params):
        Model.__init__(self, data, params)
        self.data = data
        self.params = params
        self.test_misclassifications = []
        self.train_misclassifications = []
        self.train_error_means = []
        self.test_error_means = []
        self.hidden_units_num = self.params['hidden_units']
        self.output_units_num = self.params['output_units']
        self.tolerance = self.params['tolerance']

        self.train_samples = self.data['train_data']['x']
        self.train_targets = self.data['train_data']['y']
        self.test_samples = self.data['test_data']['x']
        self.test_targets = self.data['test_data']['y']

        self.num_of_features = len(self.train_samples[0])
        self.num_of_samples = len(self.train_samples)

        # randomize weights
        self.weights_matrix_to_hidden = array([
            [uniform(0,2)-1 for i in range(self.hidden_units_num)]
            for j in range(self.num_of_features)
            ])
        self.weights_matrix_to_output = array([
            [uniform(0,2)-1 for i in range(self.output_units_num)]
            for j in range(self.hidden_units_num)
            ])
        self.hidden_layer_outputs = None
        self.output_layer_output = None

        print("Loaded Model.\nInput units: {}\nHidden units: {}\nOutput units:{}".format(len(self.train_samples[0]), self.hidden_units_num, self.output_units_num))

    def train_model(self):
        return self.train_bp()

    def evaluate_model(self):
        test_errors = self.count_errors_over_corpus(self.test_samples, self.test_targets)
        print("Model accuracy:", 1.0 - test_errors/len(self.test_samples))

    def visualize_error(self):
        plt.plot(self.train_error_means, label="train error")
        plt.plot(self.test_error_means, label="test error")
        plt.legend(loc='lower left')
        plt.title('Train & Test error')
        plt.xlabel('Epoch number')
        plt.ylabel('Error')
        plt.show()

    def visualize_misclassifications(self):
        plt.plot(self.train_misclassifications, label="train misclassiciations")
        plt.plot(self.test_misclassifications, label="test misclassiciations")
        plt.legend(loc='lower left')
        plt.title('Misclassifications')
        plt.xlabel('Epoch number')
        plt.ylabel('misclassification %')
        plt.show()

    def visualize_test_samples(self):

        # choose 25 random samples from test set
        # classify them (feed-forward)
        # visualize
        print("Visualizing 25 test sample classifications")
        samples_indexes = [randint(0,len(self.test_samples)) for i in range(25)]
        for i, s in enumerate(samples_indexes):
            sample = self.test_samples[s]
            prediction = self.feed_network(sample)

            plt.subplot(5, 5, i+1)
            plt.imshow(matrix_for_img_vector(sample))
            plt.title("8" if prediction[0] == -1 else "5")
            plt.axis('off')

        plt.show()

    def feed_network(self, sample):
        self.hidden_layer_outputs = [0] * self.hidden_units_num
        self.output_layer_output = [0] * self.output_units_num

        for h in range(self.hidden_units_num):  # feed to hidden layer
            hidden_unit_weights = self.weights_matrix_to_hidden[:, h]
            # print("Feeding sample to hidden unit {}".format(h))
            # print("Sample: {}, hidden unit weights: {}".format(sample,hidden_unit_weights))
            hidden_unit_input = dot(sample,
                                    hidden_unit_weights)
            # print("Dot product: {}".format(hidden_unit_input))

            hidden_output = sigmoid(hidden_unit_input)  # sigmoid output of hidden units
            # print("Sigmoid output: {}".format(hidden_output))

            self.hidden_layer_outputs[h] = hidden_output

        for o in range(self.output_units_num):
            # print("Feeding sample to output unit {}".format(o))

            output_unit_input = dot(self.hidden_layer_outputs,
                                 self.weights_matrix_to_output[:, o])
            # print("Dot product: {}".format(output_unit_input))
            output_unit_out = linear_output(output_unit_input)   # linear output unit
            # print("Unit output: {}".format(output_unit_out))

            self.output_layer_output[o] = output_unit_out
        return self.output_layer_output

    def error_vector_for_output_units(self, output, target):
        output_gamma_vector = [0] * self.output_units_num
        for k in range(self.output_units_num):
            # gamma_k = output[k] * (1 - output[k]) * (target[k] - output[k])
            gamma_k = (target[k] - output[k])  # Linear unit error
            output_gamma_vector[k] = gamma_k

        return output_gamma_vector

    def error_vector_for_hidden_units(self, output_units_error_vector):
        hidden_gamma_vector = [0] * self.hidden_units_num
        for h in range(self.hidden_units_num):
            hidden_unit_error_influence = dot(self.weights_matrix_to_output[h],
                                              output_units_error_vector)
            gamma_h = self.hidden_layer_outputs[h] * (1 - self.hidden_layer_outputs[h]) * hidden_unit_error_influence
            hidden_gamma_vector[h] = gamma_h

        return hidden_gamma_vector

    def count_errors_over_corpus(self, corpus_samples, corpus_targets):
        count = 0
        for s, sample in enumerate(corpus_samples):
            output_vector = self.feed_network(sample)
            if output_vector[0] != corpus_targets[s]:
                count += 1
        return count

    def train_bp(self):
        eta = self.params['eta']
        epoch_num = 0
        test_misclassification = 1.0

        while test_misclassification > self.tolerance:

            print("Epoch {}. Eta: {}". format(epoch_num, eta))

            train_epoch_errors = []
            test_epoch_errors = []

            for s, sample in enumerate(self.train_samples):
                output_vector = self.feed_network(sample)  # feed network with sample and get output
                output_gamma_vector = self.error_vector_for_output_units(output_vector, self.train_targets[s])   # calculate error gradient for output units
                hidden_gamma_vector = self.error_vector_for_hidden_units(output_gamma_vector)   # calculate error gradient for hidden units, influenced by output error

                train_output_error_mean = abs(mean(output_gamma_vector))
                train_epoch_errors.append(train_output_error_mean)

                # update weights to hidden layer
                for i in range(len(self.weights_matrix_to_hidden)):
                    for j in range(len(self.weights_matrix_to_hidden[i])):
                        gamma_j = hidden_gamma_vector[j]
                        # print(gamma_j, sample[i])
                        self.weights_matrix_to_hidden[i][j] += eta * gamma_j * sample[i]
                # print("weights to hidden after:", self.weights_matrix_to_hidden)

                # update weights to output layer
                for i in range(len(self.weights_matrix_to_output)):
                    for j in range(len(self.weights_matrix_to_output[i])):
                        gamma_j = output_gamma_vector[j]
                        self.weights_matrix_to_output[i][j] += eta * gamma_j * self.hidden_layer_outputs[i]

            test_misclassification_in_epoch = self.count_errors_over_corpus(self.test_samples, self.test_targets)
            train_misclassificaion_in_epoch = self.count_errors_over_corpus(self.train_samples, self.train_targets)
            test_misclassification = test_misclassification_in_epoch / len(self.test_samples)
            train_misclassification = train_misclassificaion_in_epoch / len(self.train_samples)
            self.train_misclassifications.append(train_misclassification)
            self.test_misclassifications.append(test_misclassification)

            train_error_mean_in_epoch = mean(train_epoch_errors)
            self.train_error_means.append(train_error_mean_in_epoch)

            # feed test set and calculate error for epoch
            for s, sample in enumerate(self.test_samples):
                output_vector = self.feed_network(sample)
                output_gamma_vector = self.error_vector_for_output_units(output_vector, self.test_targets[s])
                test_output_error_mean = abs(mean(output_gamma_vector))
                test_epoch_errors.append(test_output_error_mean)
            test_error_mean_in_epoch = mean(test_epoch_errors)
            self.test_error_means.append(test_error_mean_in_epoch)

            print("Test misclassification: {} of {}".format(test_misclassification_in_epoch, len(self.test_samples)))
            print("Train misclassification: {} of {}".format(train_misclassificaion_in_epoch, len(self.train_samples)))
            print("Test misclassification % in epoch:", test_misclassification)
            print("Train misclassification % in epoch:", train_misclassification)
            print("Train error in epoch:", train_error_mean_in_epoch)
            print("Test error in epoch:", test_error_mean_in_epoch)

            epoch_num += 1
            eta *= self.params['eta_decay']  # decay eta with each epoch

