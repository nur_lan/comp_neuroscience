from models.model import Model
import numpy as np
import random
import matplotlib.pyplot as plt


class OjaModel(Model):
    def __init__(self, data, params):
        Model.__init__(self, data, params)
        self.data = data
        self.params = params

        self.samples = self.data['samples']
        self.output_dimension = self.params['num_PCs']
        self.input_dimension = self.samples[0].shape[0]

        self.W = np.array([[random.random() for col in range(self.input_dimension)] for row in range(self.output_dimension)])
        print("Weights", self.W)

        self.compressed_samples = None

    def train_model(self):
        for epoch in range(self.params['max_iter']):
            if epoch % 100 == 0:
                print("Epoch", epoch)
                print("Weights", self.W)
                vector_sqrt = np.sqrt(np.sum(self.W[0]**2))
                print("SQRT:", vector_sqrt)
                if vector_sqrt<1.2:
                    break

            # feed samples in random order
            # random.shuffle(self.samples)
            for s, sample in enumerate(self.samples):
                y_vector = np.dot(self.W, sample)   # multiply sample by W to get y
                y_column_vector = y_vector.reshape(y_vector.shape[0], 1)

                hebbian_matrix = np.dot(y_column_vector,
                                        sample.reshape(1,sample.shape[0]))

                lagrange_vector = np.dot(y_vector, self.W)
                lagrange_row_vector = lagrange_vector.reshape(1, lagrange_vector.shape[0])

                lagrange_matrix = np.dot(y_column_vector,
                                         lagrange_row_vector)

                W_delta = self.params['eta'] * (hebbian_matrix - lagrange_matrix)
                self.W = self.W + W_delta

        print("Done training. Weights:", self.W)

    @staticmethod
    def matrix_for_flat_image(img_array, shape):
        return np.reshape(img_array, shape)

    def compress_samples(self):
        compressed_samples = []
        for sample in self.samples:
            compressed_sample = self.feed_network(sample)
            compressed_samples.append(compressed_sample)

        self.compressed_samples = compressed_samples


    def feed_network(self, input_vector):
        y_out = np.dot(self.W, input_vector)
        return y_out

    def decompress(self, compressed):
        W_transposed = self.W.T
        decompressed = np.dot(W_transposed, compressed) + self.data['mean_face']
        return decompressed

    def compress_then_decompress(self, input_vector):
        compressed = self.feed_network(input_vector)
        decompressed = self.decompress(compressed)
        return decompressed

    def evaluate_model(self):
        W_transposed = self.W.T
        # print("W transposed shape:", W_transposed.shape)

        i = 1
        for s, compressed_sample in enumerate(self.compressed_samples[:3]):
            # plt.subplot(4, 2, i)
            # # original
            # plt.imshow(self.matrix_for_flat_image(self.data['samples'][s], self.data['pic_size']), cmap=plt.gray())
            # plt.title("Meaned " + str(s))
            # plt.axis('off')

            # plot compressed representation
            plt.subplot(3, 2, i)
            plt.imshow(self.matrix_for_flat_image(self.data['samples_origin'][s], self.data['pic_size']), cmap=plt.gray())
            plt.title("Origin " + str(s))
            plt.axis('off')

            # plot compressed representation
            # plt.subplot(3, 3, i+1)
            # compressed
            # plt.imshow(self.matrix_for_flat_image(compressed_sample, (1,)), cmap=plt.gray())
            # plt.title("Compressed " + str(s))
            # plt.axis('off')

            # decompressed
            decompressed_vector = self.decompress(compressed_sample)
            plt.subplot(3, 2, i+1)
            plt.imshow(self.matrix_for_flat_image(decompressed_vector, self.data['pic_size']), cmap=plt.gray())
            plt.title("Decompressed " + str(s))
            plt.axis('off')

            i += 2

        plt.show()




