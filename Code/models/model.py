import pickle

class Model:

    def __init__(self, data, parameters):
        pass

    def evaluate_model(self):
        pass

    def train_model(self):
        pass

    def visualize_error(self):
        pass


    @classmethod
    def save_model_to_file(cls, model, filename):
        f = open(filename, mode='wb')
        pickle.dump(model, file=f)
        print("Saved model to file", filename)

    @classmethod
    def load_model_from_file(cls, load_file):
        model = pickle.load(open(load_file, 'rb'))
        print("Loaded models from", load_file)
        return model

