from models.model import Model
from random import random, shuffle
import numpy as np
from math import sqrt, log
import matplotlib.pyplot as plt
from multiprocessing import Pool
import pickle

###############
# hyper params
###############

LOAD_WEIGHTS_FROM_FILE = True
WEIGHTS_FILE = "./bin_dumps/som.bin"

NUM_SAMPLES = 200
NUM_SOM_UNITS = 15
LATTICE_COLUMNS = 5
LATTICE_ROWS = int(NUM_SOM_UNITS / LATTICE_COLUMNS)
INPUT_DIMENSION = 2

SO_MAX_EPOCH = 6000
CONVERGENCE_MAX_EPOCH = 500*20
TOLERANCE = 1e-4

SIGMA_0 = NUM_SOM_UNITS/2
TAU_SIGMA = 1000/log(SIGMA_0)
ETA_0 = 0.1
TAU_ETA = -1 * SO_MAX_EPOCH/log(0.001/ETA_0)

SIGMA = SIGMA_0
ETA = ETA_0


###############
# helpers
###############


def lattice_distance(coord_1, coord2):
    a = np.array([coord_1[0], coord_1[1]])
    b = np.array([coord2[0], coord2[1]])
    return np.linalg.norm(a-b)

def weights_distance(arr1, arr2):
    diff_arr = arr1 - arr2
    distance = np.linalg.norm(diff_arr)
    return distance


def gaussian(d, sigma):
    left = (sqrt(2 * sigma**2 * np.math.pi)) ** -1
    left = 1
    return left * np.math.e ** ((-1 * (d**2)) / (2 * (sigma**2)))


def coord_for_unit_index(index):
    row = int(index / LATTICE_ROWS)
    column = index % LATTICE_COLUMNS
    return row, column


def index_for_coord(coord_tuple):
    row = coord_tuple[0]
    column = coord_tuple[1]
    index = row * LATTICE_COLUMNS + column
    return index


def eta_at_time(t):
    return ETA_0 * np.math.e ** ((-1 * t) / TAU_ETA)


def sigma_at_time(t):
    return SIGMA_0 * np.math.e ** ((-1 * t) / TAU_SIGMA)



class Self_Organizing_Map_Model(Model):
    def __init__(self, data=None, params=None, second_dataset=False):
        Model.__init__(self, data, params)
        self.ETA = ETA
        self.SIGMA = SIGMA
        self.second_dataset = second_dataset

        ###############
        # generate samples
        ###############
        if second_dataset:
            eye_1_x = -0.5
            eye_1_y = 0.5

            eye_2_x = 0.5
            eye_2_y = 0.5

            RAND_NORM = 7
            RAND_RADIUS = 0.5

            eye_1_x_values = [eye_1_x + ((random() - RAND_RADIUS) / RAND_NORM) for x in range(int(NUM_SAMPLES / 4))]
            eye_1_y_values = [eye_1_y + ((random() - RAND_RADIUS) / RAND_NORM) for x in range(int(NUM_SAMPLES / 4))]

            eye_2_x_values = [eye_2_x + ((random() - RAND_RADIUS) / RAND_NORM) for x in range(int(NUM_SAMPLES / 4))]
            eye_2_y_values = [eye_2_y + ((random() - RAND_RADIUS) / RAND_NORM) for x in range(int(NUM_SAMPLES / 4))]

            smile_x = [random()-0.5 for x in range(int(NUM_SAMPLES/2))]
            smile_y = [-1 * sqrt(0.25 - x**2) for x in smile_x]

            self.x_values = eye_1_x_values + eye_2_x_values + smile_x
            self.y_values = eye_1_y_values + eye_2_y_values + smile_y


        else:
            self.x_values = [(random() * 2) - 1 for x in range(int(NUM_SAMPLES / 2))]
            self.y_values = [sqrt(1 - (x ** 2)) + 0.05 * random() for x in self.x_values]

            x_2 = [(random() * 2) - 1 for x in range(int(NUM_SAMPLES / 2))]
            y_2 = [-1 * sqrt(1 - (x ** 2)) + 0.05 * random() for x in x_2]

            self.x_values.extend(x_2)
            self.y_values.extend(y_2)

        ###############
        # init weights
        ###############

        self.weights = np.zeros((NUM_SOM_UNITS, INPUT_DIMENSION))
        for x in range(NUM_SOM_UNITS):
            for y in range(INPUT_DIMENSION):
                self.weights[x, y] = random()
        print("Initialized random weights:", self.weights)

        self.samples = []
        for s in range(len(self.x_values)):
            sample = np.array([self.x_values[s], self.y_values[s]])
            self.samples.append(sample)

        print("Samples:", self.samples)
        # self.plot_weights("Init")


    def plot_weights(self, title="Samples (blue) and weights(red)"):
        plt.scatter(self.x_values, self.y_values, color='blue')
        # plt.xlabel('x')
        # plt.ylabel('y')
        # plt.show()

        plt.scatter(self.weights[:, 0], self.weights[:, 1], color='red')
        plt.title(title)
        plt.xlabel('x')
        plt.ylabel('y')
        plt.pause(.1)
        # plt.ion()
        plt.show()
        # plt.close()
    #
    # ###############
    # # init weights
    # ###############
    #
    # weights = np.zeros((NUM_SOM_UNITS, INPUT_DIMENSION))
    # for x in range(NUM_SOM_UNITS):
    #     for y in range(INPUT_DIMENSION):
    #         weights[x,y] = random()
    # print("Initialized random weights:", weights)

    def train_model(self):

        ###############
        # main algorithm
        ###############

        for epoch in range(SO_MAX_EPOCH):

            if epoch % 10 == 0:
                print(epoch)

            if epoch % 3000 == 0:
                print("ETA:", self.ETA)
                print("SIGMA:", self.SIGMA)
                print(epoch)
                # self.plot_weights("Samples (blue) and weights(red) after {} epochs".format(str(epoch)))

            sample_indeces = list(range(NUM_SAMPLES))
            shuffle(sample_indeces)
            for sample_index in sample_indeces:  # TODO CHOOSE RANDOMLY
                sample = self.samples[sample_index]

                min_distance = 2 ** 32
                best_matching_unit_index = 0
                for unit_index in range(NUM_SOM_UNITS):
                    unit_weights = self.weights[unit_index]
                    distance_to_input = weights_distance(sample, unit_weights)
                    if distance_to_input < min_distance:
                        min_distance = distance_to_input
                        best_matching_unit_index = unit_index

                best_matching_unit_coord = coord_for_unit_index(best_matching_unit_index)

                for unit_index in range(NUM_SOM_UNITS):
                    unit_weights = self.weights[unit_index]
                    x, y = coord_for_unit_index(unit_index)
                    distance_from_bmu = lattice_distance((x, y), best_matching_unit_coord)
                    self.weights[unit_index] = unit_weights + self.ETA * gaussian(distance_from_bmu, self.SIGMA) * (sample - unit_weights)

            self.ETA = eta_at_time(epoch)
            self.SIGMA = sigma_at_time(epoch)
