from datasets import datasets
from settings import params_pla, params_bp
from utils.utils import explore_data, visualize_weights
from models.model import Model
from models.PLA import PLAModel
from models.backpropagation import BackPropagationModel


if __name__ == '__main__':
    data = datasets.get_dataset('MNIST')

    # PLA
    # explore_data(data['train_data']['x'])
    # pla_model = PLAModel(data, params_pla)
    # pla_model.train_model()
    # pla_model.evaluate_model(visualize=False)

    # BackProp
    # bp_model = BackPropagationModel(data, params_bp)
    # bp_model.train_model()
    # Model.save_model_to_file(bp_model, 'bp_model_5')
    #
    bp_model = Model.load_model_from_file('bp_model_5')
    bp_model.visualize_error()
    bp_model.visualize_misclassifications()
    bp_model.evaluate_model()
    bp_model.visualize_test_samples()

    # pla_model.evaluate_model(visualize=False)

    # visualize_weights(pla_model.w)
