from os import path, listdir
import scipy.io as sio
import scipy.ndimage
from settings import PATH2DATA, params_hopfield
import numpy as np
from random import choice
from utils.utils import standardize_samples
from PIL import Image

OR = {
    'train_data':
        {
            'x': [
                [0, 0, 1],
                [1, 0, 1],
                [0, 1, 1],
                [1, 1, 1]
            ],
            'y': [-1, 1, 1, 1]
        },
    'test_data':
        {
            'x': [
                [0, 0, 1],
                [1, 0, 1]
                ],
            'y': [-1,1]
        }
}

AND = {
    'train_data': {

        'x': [
            [0, 0, 1],
            [1, 0, 1],
            [0, 1, 1],
            [1, 1, 1]
        ],
        'y': [-1, -1, -1, 1]
    }

}

XOR = {
    'train_data': {
        'x': [
            [0, 0, 1],
            [1, 0, 1],
            [0, 1, 1],
            [1, 1, 1]
        ],
        'y': [-1, 1, 1, -1]
    }
}

HOPFIELD_MEMORIES = {
    'train':

        {
            'E': [
                1, 1, 1, 1, 1,
                1, -1, -1, -1, -1,
                1, 1,  1,  1, 1,
                1, -1, -1, -1, -1,
                1, 1, 1, 1, 1
            ],
            'H': [
                1, -1, -1, -1, 1,
                -1,  1, -1,  1, -1,
                -1, -1,  1, -1, -1,
                -1, 1, -1,  1, -1,
                1, -1, -1, -1, 1
            ],
            'I':
                [
                    -1,  1,  1,  1, -1,
                    -1, -1,  1, -1, -1,
                    -1, -1,  1, -1, -1,
                    -1, -1, 1, -1, -1,
                    -1,  1,  1,  1, -1
                ]
        },

    'test':
        {
            'E1': [
                [1, 1, 1, 1, 1],
                [1, 1, -1, -1, -1],
                [1, 1,  1,  1, 1],
                [1, 1, -1, -1, -1],
                [1, 1, 1, 1, 1]
            ],

            'E2': [
                [1, 1, 1, 1, 1],
                [1, 1, 1, -1, -1],
                [1, 1, 1,  1, 1],
                [1, 1, 1, -1, -1],
                [1, 1, 1, 1, 1]
            ],

            'I1':
            [
                [1,  1,  1,  1, -1],
                [1, -1,  1, -1, -1],
                [1, -1,  1, -1, -1],
                [1, -1, 1, -1, -1],
                [1,  1,  1,  1, -1]
            ]
        }
}


def get_mnist_data():
    data_train_filename = path.join(PATH2DATA, 'dataset_train.mat') # Load MNIST datasets from matlab files
    data_test_filename = path.join(PATH2DATA, 'dataset_test.mat')
    train_dict = sio.loadmat(data_train_filename)
    test_dict = sio.loadmat(data_test_filename)

    data_dict = {
        'train_data': {
            'x': train_dict['X_train'],
            'y': train_dict['y_train']
        },
        'test_data': {
            'x': test_dict['X_test'],
            'y': test_dict['y_test']
        }
    }
    data_dict['train_data']['x'] = standardize_samples(data_dict['train_data']['x'])
    data_dict['test_data']['x'] = standardize_samples(data_dict['test_data']['x'])
    return data_dict


def get_random_hopfield_memories(num):
    num_of_units = params_hopfield['num_of_units']
    # shape = (int(sqrt(num_of_units)), int(sqrt(num_of_units)))
    memories_train = {}
    memories = {}
    for i in range(num):
        mem = [choice([-1,1]) for y in range(num_of_units)]
        memories_train[str(i)] = mem
    memories['train'] = memories_train
    memories['test'] = {}
    # print("Generated random mems:", memories)
    return memories


def generate_competetive_samples(x_dimension, num_of_clusters, num_of_cluster_samples):
    samples = []
    for cluster in range(num_of_clusters):
        cluster_angle = np.random.ranf() * (2 * np.pi)
        sample_variances = [np.random.sample(size=2)/10 for s in range(num_of_cluster_samples)]
        # print("variances", sample_variances)
        cluster_samples = [
            [np.cos(cluster_angle) + sample_var[0], np.sin(cluster_angle) + sample_var[1]] for sample_var in sample_variances]
        samples.extend(cluster_samples)
    # print("samples:", samples)

    return samples


def get_faces_data():
    faces_data_path = path.join(PATH2DATA, 'faces_data')
    samples_origin = []
    file_names = []
    for filename in listdir(faces_data_path):
        if not filename.endswith('.centerlight'):
            continue
        full_filename = path.join(PATH2DATA, 'faces_data', filename)

        face_image = Image.open(full_filename)
        face_data = np.array(face_image.getdata(), np.float)
        samples_origin.append(face_data)
        file_names.append(full_filename)
    return_dict = {
        'file_names': file_names,
        'samples_origin':samples_origin
    }
    return return_dict




def get_dataset(dataset_name, num_of_random_samples=None):
    if dataset_name == 'OR':
        return OR
    elif dataset_name == 'AND':
        return AND
    elif dataset_name == 'XOR':
        return XOR
    elif dataset_name == 'MNIST':
        return get_mnist_data()
    elif dataset_name == 'HOPFIELD_MEMORIES':
        if num_of_random_samples:
            return get_random_hopfield_memories(num_of_random_samples)
        else:
            return HOPFIELD_MEMORIES
    else:
        print("Unknown datasets type")
        raise Exception


