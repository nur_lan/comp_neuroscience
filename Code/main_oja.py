from numpy.random import rand
from scipy.stats import multivariate_normal

def load_settings_params():
    pass


def generate_2dGaussiandata():
    pass


def train_model():
    pass


def draw_eigenvectors():
    pass


params = {
    'eta': 1e-2,
    'mu': [0, 0],
    'sigma': [
        [4, 2],
        [2, 4]
    ],
    'num_of_samples': 500,
    'max_iter': 1000,
    'seed': 1
}


if __name__ == '__main__':
    #
    # samples = multivariate_normal(params['mu'], cov=params['sigma'], size=params['num_of_samples'])
    rands = [[rand(), rand()] for x in range(params['num_of_samples'])]
    samples = multivariate_normal(mean=params['mu'], cov=params['sigma'])

    print(samples.pdf(rands))

    # rands = [rand() for x in range(params['num_of_samples'])]
    # y = multivariate_normal.pdf(rands, mean=params['mu'], cov=params['sigma'])
    # print(y)
    print(samples)



# multivariate_normal.pdf()

