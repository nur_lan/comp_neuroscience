from random import randrange
from matplotlib import pyplot as plt
from utils.utils import plot_weights

board_size = 8
num_cells = pow(board_size,2)
weights = None
cells = [0 for i in range(num_cells)]
threshold = None
iterations = num_cells*10

#### helper functions, not interesting ####

def plotBoard(units):
  pass

def printBoard(units):
  res = ""
  for i in range(len(units)):
    res += " ." if units[i] < 0 else " *"
    if i % board_size == board_size - 1:
      res += "\n"
  print(res)

def randomInitialSetup(): # randomly generated starting position
  units = []
  for i in range(num_cells):
    units.append(1 if randrange(0,2) == 1 else -1)
        
  return units

def setWeights(): # sets weights and threshold
  global weights, threshold
  
  weights = [[0]*num_cells for i in range(num_cells)]

  for i in range(num_cells): # for every cell
    for x in range(i+1, num_cells): # loop through all later cells
      if i % board_size == x % board_size: # if they are on the same column
        weights[i][x] = -2
        weights[x][i] = -2
      if x - i + i % board_size < board_size: # on the same row
        weights[i][x] = -2
        weights[x][i] = -2

  threshold = (board_size * 2 - 1)*2 - 4

#### main algorithm ####

def runNetwork():
  def checkCell(i):
    s = 0
    
    for j in range(num_cells):
      s += weights[j][i]*cells[j]

    return 1 if s > threshold else -1

  for t in range(iterations): # update cells
    i = randrange(0, num_cells)
    cells[i] = checkCell(i)
  
  return cells


#######################

#setup = [-1 for i in range(8) for x in range(8)] # empty board
setup = randomInitialSetup()
# print("\n8 Rooks: A random initial setup of " + str(len(setup)) + " units:\n")
# printBoard(setup)
plot_weights(setup, (8,8), "8 Rooks: A random initial setup of " + str(len(setup)) + " units" )

# print("\nNetwork state after " + str(iterations) + " iterations:\n")
setWeights()
network = runNetwork()
print(network)

plot_weights(network, (8,8), "8 Rooks: Network state after " + str(iterations) + " iterations")