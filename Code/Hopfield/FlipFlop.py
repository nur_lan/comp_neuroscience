from random import randrange
from utils.utils import plot_weights

num_cells = None
weights = None
cells = None
threshold = None
iterations = None

#### helper functions, not interesting ####

def printCells(units): # prints a network state
  res = ""
  for i in range(len(units)):
    res += " -" if units[i] < 0 else " *"
  print(res)

def randomInitialSetup(num_cells): # randomly generated starting position
  units = []
  for i in range(num_cells):
    units.append(1 if randrange(0,2) == 1 else -1)
        
  return units

#### main algorithm ####

def setWeights(): # sets weights to -2
  for i in range(num_cells):
    for j in range(i):
      weights[i][j] = -2
      weights[j][i] = -2 # symmetricity

def initNetwork(setup): # sets parameters and initial position of the network
  global num_cells, weights, cells, threshold, iterations
  num_cells = len(setup)
  weights = [[0]*num_cells for i in range(num_cells)]
  cells = []
  threshold = 2*num_cells - 4
  iterations = 10*num_cells

  setWeights()
  for i in range(num_cells):
    cells.append(setup[i])

def runNetwork():
  def checkCell(i):
    s = 0
    
    for j in range(num_cells):
      s += weights[j][i]*cells[j]

    return 1 if s > threshold else -1

  for t in range(iterations): # update cells
    i = randrange(0, num_cells)
    cells[i] = checkCell(i)
  
  return cells


#######################

setup = randomInitialSetup(25)
# print("\nFlip Flop: A random initial setup of " + str(len(setup)) + " units:\n")
# printCells(setup)
plot_weights(setup, (5,5), "Flip Flop: A random initial setup of " + str(len(setup)) + " units")

initNetwork(setup)
# print("\nNetwork state after " + str(iterations) + " iterations:\n")
# printCells(runNetwork())
network = runNetwork()
plot_weights(network, (5,5), "Flip Flop: Network state after " + str(iterations) + " iterations")