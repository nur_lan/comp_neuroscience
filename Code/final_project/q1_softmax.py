import tensorflow as tf
import numpy as np


def softmax(x):
    maximums = tf.reduce_max(x, reduction_indices=[1])
    maximums_reshaped_for_substraction = tf.expand_dims(maximums, 1)
    x_after_numerical_stability_substraction = x - maximums_reshaped_for_substraction  # See http://timvieira.github.io/blog/post/2014/02/11/exp-normalize-trick/
    nominator = tf.exp(x_after_numerical_stability_substraction)

    sums = tf.reduce_sum(nominator, reduction_indices=[1])
    sums_reshaped_for_division = tf.expand_dims(sums, 1)
    denominator = sums_reshaped_for_division

    out = nominator / denominator
    return out


def cross_entropy_loss(y, yhat):
    y_target_float = tf.to_float(y)
    yhat_log = tf.log(yhat)
    mul = tf.mul(y_target_float, yhat_log)
    sums = -1.0 * tf.reduce_sum(mul)
    return sums


def test_softmax_basic():
    """
    Some simple tests to get you started.
    Warning: these are not exhaustive.
    """
    print("Running basic tests...")
    test1 = softmax(tf.convert_to_tensor(
        np.array([[1001, 1002], [3, 4]]), dtype=tf.float32))
    with tf.Session():
        test1 = test1.eval()

    val = np.amax(np.fabs(test1 - np.array(
        [0.26894142, 0.73105858])))
    assert val <= 1e-6

    test2 = softmax(tf.convert_to_tensor(
        np.array([[-1001, -1002]]), dtype=tf.float32))
    with tf.Session():
        test2 = test2.eval()
    assert np.amax(np.fabs(test2 - np.array(
        [0.73105858, 0.26894142]))) <= 1e-6


def test_cross_entropy_loss_basic():
    """
    Some simple tests to get you started.
    Warning: these are not exhaustive.
    """
    y = np.array([[0, 1], [1, 0], [1, 0]])
    yhat = np.array([[.5, .5], [.5, .5], [.5, .5]])

    test1 = cross_entropy_loss(
        tf.convert_to_tensor(y, dtype=tf.int32),
        tf.convert_to_tensor(yhat, dtype=tf.float32))
    with tf.Session():
        test1 = test1.eval()
    result = -3 * np.log(.5)
    assert np.amax(np.fabs(test1 - result)) <= 1e-6
    print("Basic (non-exhaustive) cross-entropy tests pass\n")


if __name__ == "__main__":
    test_softmax_basic()
    test_cross_entropy_loss_basic()

test_softmax_basic()
test_cross_entropy_loss_basic()
