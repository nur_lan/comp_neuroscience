import matplotlib.pyplot as plt
import numpy as np
from scipy import integrate

# ROMEO - cautious
a = -0.1     # trust in himself
b = 1 # trust in juliet
# JULIET

C = 1   # trust in romeo
D = .1  # trust in herself

r0 = 2
j0 = 5

juliet_strategies = {   # title: (c,d)
    "Eager Beaver": (C, D),
    "Narcissistic nerd": (-C, D),
    "Cautious lover": (C, -D),
    "Hermit": (-C, -D)
}
strategies = sorted(juliet_strategies.keys())

def X_r_j(X, t=0):
    x_r = X[0]
    x_j = X[1]
    return np.array([a * x_r + b * x_j, c * x_r + d * x_j])

PHASE=True

if PHASE:

    R, J = np.meshgrid(np.arange(-10, 10, 1), np.arange(-10, 10, 1))

    dR = np.zeros(R.shape)
    dJ = np.zeros(J.shape)

    NI, NJ = dR.shape

    plt_num = 1
    plt.suptitle("dJ, dR phase portrait over JR plane. a={}, b={}".format(a,b), fontsize=16)

    for strat in strategies:
        c, d = juliet_strategies[strat]

        for i in range(NI):
            for k in range(NJ):
                r = R[i, k]
                j = J[i, k]
                love = X_r_j([r, j])
                dR[i, k] = love[0]
                dJ[i, k] = love[1]

        # dR = a*R + b*J
        # dJ = c*R + d*J

        plt.subplot(2, 2, plt_num)
        plt.xlabel("R", fontsize=10)
        plt.ylabel("J", fontsize=10)
        plt.quiver(R, J, dR, dJ)
        plt.title(strat + " Juliet")

        # plot trajectory
        X0 = np.array([r0, j0])
        t = np.linspace(0, 100, 200)

        X = integrate.odeint(X_r_j, X0, t)
        romeos = X[:,0]
        juliets = X[:,1]
        # romeos, juliets = X.T
        # print(romeos, juliets)
        plt.plot(romeos, juliets, '-g')
        plt.plot([X[0, 0]], [X[0, 1]], 'o')  # start
        plt.plot([X[-1, 0]], [X[-1, 1]], 's')  # end

        plt_num += 1

    plt.show()


###
#ODE
###

r0 = 2
j0 = 5

plt_num = 1

for strat in strategies:
    print(strat)
    r_list = []
    j_list = []

    c, d = juliet_strategies[strat]

    X0 = np.array([r0, j0])
    t = np.array(list(range(100)))

    X = integrate.odeint(X_r_j, X0, t)
    romeos, juliets = X.T
    print(romeos, juliets)

    PLOT = 0

    if PLOT == 0:
        plt.suptitle("Trajectory: a={} b={}".format(a, b), fontsize=16)

        plt.subplot(2, 2, plt_num)
        plt.xlabel("R", fontsize=10)
        plt.ylabel("J", fontsize=10)
        plt.xticks([min(romeos), max(romeos)])
        plt.xticks([min(juliets), max(juliets)])
        # plt.axis('off')
        plt.plot([romeos[0], juliets[0]], 'o')  # start
        plt.plot([romeos[-1], juliets[-1]], 's')  # end
        plt.plot(romeos, juliets, '-g')


    if PLOT == 1:
        plt.suptitle("J, R over time: a={} b={}".format(a, b), fontsize=16)

        plt.subplot(2, 2, plt_num)
        plt.xlabel("time", fontsize=10)
        plt.ylabel("love", fontsize=10)
        plt.plot(t, juliets, '-b', label='Juliet')
        plt.plot(t, romeos, '-r', label='Romeo')
        # plt.axis('off')
        plt.legend(loc='best')

    plt.title(strat + " Juliet")
    plt_num += 1

plt.show()

"""
plt.figure()
Q = plt.quiver(dR, dJ)
l, r, b, t = plt.axis()
dx, dy = r - l, t - b
plt.axis([l - 0.05*dx, r + 0.05*dx, b - 0.05*dy, t + 0.05*dy])

plt.title('Minimal arguments, no kwargs')
plt.pause(.1)
# plt.ion()
plt.show()
"""
