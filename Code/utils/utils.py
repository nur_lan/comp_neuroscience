from math import e, sqrt
from random import shuffle
from statistics import pstdev
import numpy as np
from numpy import mean, reshape, array
from sklearn.preprocessing import scale
import matplotlib.pyplot as plt


def sign(n):
    return 1 if n > 0 else -1


def sigmoid(a, beta=1):
    return (1 + e**(-1 * a * beta)) ** -1


def linear_output(a):
    # return sigmoid(a)
    return sign(a)


def visualize_classification(correct_samples, incorrect_samples):

    for c, sample_classification in enumerate(correct_samples):
        sample = sample_classification[0]
        sample_classification = sample_classification[1]

        plt.subplot(5, 6, c+1)
        plt.imshow(matrix_for_img_vector(sample))
        plt.title(sample_classification)
        plt.axis('off')

    for i, sample_classification in enumerate(incorrect_samples):
        sample = sample_classification[0]
        sample_classification = sample_classification[1]

        plt.subplot(5, 6, i+len(correct_samples)+1)
        plt.imshow(matrix_for_img_vector(sample))
        plt.title(sample_classification)
        plt.axis('off')

    plt.show()


def plot_error(errors, title="Train error"):

    average_error = [0] * len(errors[0])

    for i in range(len(errors[0])):
        average_error[i] = mean([e[i] if i<len(e) else 0 for e in errors])

    plt.plot(average_error)
    plt.title(title)
    plt.xlabel('Epoch number')
    plt.ylabel('Classification errors')
    plt.show()


def explore_data(data):
    samples_indexes = list(range(len(data)))
    shuffle(samples_indexes)

    for index_abs, index_rand in enumerate(samples_indexes[:30]):
        sample = data[index_rand]
        plt.subplot(5, 6, index_abs+1)
        plt.imshow(matrix_for_img_vector(sample))
        plt.axis('off')

    plt.show()


def standardize_samples(samples):
    samples = array(samples).astype(float)  # convert samples to float
    print('Normalizing samples.')
    return scale(samples)


def standardize_samples_2(samples):
    standard_devs = [0] * len(samples[0])
    means = [0] * len(samples[0])

    for f in range(len(samples[0])):
        f_vector = [sample[f] for sample in samples]
        f_mean = mean(f_vector)
        f_standard_dev = pstdev(f_vector)
        standard_devs[f] = f_standard_dev
        means[f] = f_mean

    for sample in samples:
        for f in range(len(sample)):
            sample[f] = (sample[f] - means[f]) / standard_devs[f]


def matrix_for_img_vector(img_vector, width=None, height=None):
    if not width:
        width = height = int(sqrt(len(img_vector)))
    return reshape(img_vector, newshape=[width, height])


def show_image(img_arr, grayscale=False):
    img_matrix = matrix_for_img_vector(img_arr)
    imgplot = plt.imshow(img_matrix, vmin=0, vmax=256, cmap=plt.get_cmap('gray') if grayscale else None)
    plt.show()

def visualize_weights(weights):
        show_image(weights, grayscale=True)

def plot_weights(weights, shape, title="Weights"):
    for i, w in enumerate(weights):
        if w<0:
            weights[i] = 0
    plt.imshow(np.abs(np.array(weights).reshape(shape)), interpolation='nearest',
                   cmap='binary', vmax=1, vmin=0)
    plt.title(title)
    plt.xticks(())
    plt.yticks(())
    plt.show()
