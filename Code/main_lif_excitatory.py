from datasets import datasets
from utils.utils import explore_data, visualize_weights
import numpy as np
import matplotlib.pyplot as plt
from brian2 import *

prefs.codegen.target = "numpy"

TAU_MEMBRANE = 10 * ms
TAU_EXCITATORY = 2.5 * ms
TAU_INHIBITATORY = 5 * ms
V_THRESHOLD = -50 * mV
V_RESET = -70 * mV
EL = -49 * mV

eqs = '''
dv/dt  = (ge+gi-(v-EL))/TAU_MEMBRANE : volt (unless refractory)
dge/dt = -ge/TAU_EXCITATORY : volt (unless refractory)
dgi/dt = -gi/TAU_INHIBITATORY : volt (unless refractory)
'''

P = NeuronGroup(100, eqs, threshold='v>V_THRESHOLD', reset='v = V_RESET', refractory=1 * ms,
                method='linear')
P.v = 'V_RESET + rand() * (V_THRESHOLD - V_RESET)'
P.ge = 0 * mV
P.gi = 0 * mV


CONN_E2E = 0.05 * mV
CONN_E2I = 0.2 * mV
CONN_I2E = 0.8 * mV
CONN_I2I = 0.0 * mV

# we = (60 * 0.27 / 10) * mV  # excitatory synaptic weight (voltage)
# wi = (-20 * 4.5 / 10) * mV  # inhibitory synaptic weight

P_E = P[:20]
P_I = P[20:]

C_E2E = Synapses(P_E, P_E, on_pre='ge += CONN_E2E')
C_E2I = Synapses(P_E, P_I, on_pre='gi += CONN_E2I')
C_I2E = Synapses(P_I, P_E, on_pre='ge += CONN_I2E')
C_I2I = Synapses(P_I, P_I, on_pre='gi += CONN_I2I')

C_E2E.connect(p=0.05)
C_E2I.connect(p=0.2)
C_I2E.connect(p=0.8)
C_I2I.connect(p=0.6)

s_mon = SpikeMonitor(P)
state_mon = StateMonitor(P, 'v', record=True)

run(0.3 * second)

PLOT_RASTER = True

if PLOT_RASTER:
    plot(s_mon.t / ms, s_mon.i, '.k')
    title("Raster for network of 20 excitatory, 80 inhibitory neurons")
    xlabel('Time (ms)')
    ylabel('Neuron')
    show()

else:

    e, = plot(state_mon.t / ms, state_mon.v[0], '-r', label="Excitatory neuron")
    i, = plot(state_mon.t / ms, state_mon.v[25], '-b', label="Inhibitory neuron")
    xlabel('Time (ms)')
    ylabel('Membrane potential (mV)')
    legend([e,i], ['Excitatory neuron', 'Inhibitory neuron'])
    axes = gca()
    axes.set_ylim([-0.071,-0.049])
    show()
