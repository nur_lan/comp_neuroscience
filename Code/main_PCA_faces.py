from models.oja import OjaModel
import numpy as np
import matplotlib.pyplot as plt
from datasets.datasets import get_faces_data
import random

params = {
    'eta': 1e-12,
    'num_PCs': 10,
    'max_iter': 100000,
    'compressed_shape': (1, )
}


def visualize_samples_and_mean(data):
    samples_origin = data['samples_origin']
    samples_to_visualize = random.sample(samples_origin, 3)
    samples_to_visualize.append(data['mean_face'])

    shape = data['pic_size']
    for s, sample in enumerate(samples_to_visualize):
        plt.subplot(2, 2, s+1)
        plt.imshow(OjaModel.matrix_for_flat_image(sample, shape), cmap=plt.gray())
        plt.title("Face" + str(s+1) if s+1 != 4 else "Mean face")
        plt.axis('off')
    plt.show()


def load_settings_params():
    pass


def load_faces_data():
    data_dict = get_faces_data()
    samples_origin = data_dict['samples_origin']

    data_dict['mean_face'] = np.mean(samples_origin, axis=0)
    data_dict['pic_size'] = (243, -1)

    samples = [sample - data_dict['mean_face'] for sample in samples_origin]
    data_dict['samples'] = samples

    return data_dict

def train_model():
    pass

def eval_model():
    pass


def evaluate_all(data):
    evaluate_models_files = [
       './bin_dumps/oja_model_6000_1', './bin_dumps/oja_model_20000_3', './bin_dumps/oja_model_100000_10'
    ]
    num_of_samples_to_demo = 6
    evaluate_models = []
    for current_model_file in evaluate_models_files:
        new_model = OjaModel.load_model_from_file(current_model_file)
        evaluate_models.append(new_model)

    plot_row = 0
    model_num = 1
    for n in range(num_of_samples_to_demo):

        sample = data['samples_origin'][n]

        # original
        plt.subplot(num_of_samples_to_demo, len(evaluate_models) + 1, plot_row + 1)
        plt.imshow(OjaModel.matrix_for_flat_image(sample, data['pic_size']), cmap=plt.gray())
        plt.title("Original " + str(model_num))
        plt.axis('off')

        for m, model in enumerate(evaluate_models):
            compressed = model.compressed_samples[n]
            out_vector = model.decompress(compressed)
            reconstructed_img = OjaModel.matrix_for_flat_image(out_vector, data['pic_size'])

            plt.subplot(num_of_samples_to_demo, len(evaluate_models) + 1, plot_row + 2 + m)
            plt.imshow(reconstructed_img, cmap=plt.gray())
            plt.title('{} PCs'.format(model.params['num_PCs']))
            plt.axis('off')

        plot_row += len(evaluate_models) + 1
        model_num += 1

    plt.show()


if __name__ == '__main__':

    LOAD_MODEL_FROM_BIN = True
    LOAD_DATA_FROM_BIN = True

    if LOAD_DATA_FROM_BIN:
        data = OjaModel.load_model_from_file('./bin_dumps/oja_data.bin')
    else:
        data = load_faces_data()
        OjaModel.save_model_to_file(data, './bin_dumps/oja_data.bin')
    # visualize_samples_and_mean(data)

    model_file = './bin_dumps/oja_model_{}_{}'.format(params['max_iter'], params['num_PCs'])

    evaluate_all(data)
    # exit()

    if LOAD_MODEL_FROM_BIN:
        oja_model = OjaModel.load_model_from_file(model_file)
    else:
        oja_model = OjaModel(data, params)
        oja_model.train_model()
        oja_model.compress_samples()
        OjaModel.save_model_to_file(oja_model, model_file)

    # feed_and_visualize(oja_model, data)

    # tst_dot = np.dot(oja_model.W[2], oja_model.W[1])
    # print("Dot:", tst_dot)
    # exit()

    oja_model.evaluate_model()

