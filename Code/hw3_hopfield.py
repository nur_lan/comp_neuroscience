from datasets import datasets
from settings import params_hopfield
from models.hopfield import HopfieldModel
from random import seed
import numpy as np
import matplotlib.pyplot as plt


def visualize_stats(model):

    alphas_by_units = model.statistics['alphas']
    units_lengths = model.statistics['unit_lengths']

    alpha_means = []
    alpha_stds = []

    for alphas in alphas_by_units:
        alpha_mean = np.mean(alphas)
        alpha_means.append(alpha_mean)
        alpha_std = np.std(alphas)
        alpha_stds.append(alpha_std)

    plt.figure()
    plt.xlabel('Number of units (n)')
    plt.ylabel('alpha = N_max/n')

    plt.errorbar(units_lengths, alpha_means, xerr=None, yerr=alpha_stds)
    plt.xticks(np.arange(0, 600, 50))

    plt.title("Alpha w.r.t number of units")
    plt.show()


def find_N_max():
    NUM_OF_SEEDS = 10

    params = params_hopfield
    model = None

    units_lengths = [10, 50, 100, 150, 200, 250, 300, 500]
    alphas_by_units = []

    for num_of_units in units_lengths:
        params['num_of_units'] = num_of_units
        print("Units: ", num_of_units)

        alphas_by_seed = []
        for randseed in range(NUM_OF_SEEDS):
            print("Seed ", randseed)
            seed(randseed)
            num_memories = 0
            recall_rate = 1.0
            while recall_rate == 1.0:
                num_memories += 1
                data = datasets.get_dataset('HOPFIELD_MEMORIES', num_of_random_samples=num_memories)
                model = HopfieldModel(data, params)
                model.train_model()
                recall_rate = model.evaluate_model()
                # print("\t{} memories -- Recall {}".format(num_memories,recall_rate))
            max_n = num_memories
            print("Units {}, MAX_N = {}".format(num_of_units, max_n))
            alpha = max_n / num_of_units
            alphas_by_seed.append(alpha)

        alphas_by_units.append(alphas_by_seed)

    model.statistics['unit_lengths'] = units_lengths
    model.statistics['alphas'] = alphas_by_units

    HopfieldModel.save_model_to_file(model, './bin_dumps/hopfield_model_n_max_3.bin')
    return model


def main():
    data = datasets.get_dataset('HOPFIELD_MEMORIES', num_of_random_samples=params_hopfield['num_memories'])
    params = params_hopfield

    model = HopfieldModel(data, params)

    model.train_model()
    model.evaluate_model()

    # model.show_all_training_set()
    # model.show_all_test_set()
    HopfieldModel.save_model_to_file(model, './bin_dumps/hopfield_model_1.bin')


if __name__ == '__main__':
    # model = find_N_max()
    model = HopfieldModel.load_model_from_file('./bin_dumps/hopfield_model_n_max_3.bin')
    visualize_stats(model)
