from brian2 import NeuronGroup, mV, ms, volt
from brian2 import ohm, SpikeMonitor, StateMonitor, run, second, Synapses, prefs, rand
import matplotlib.pyplot as plt
from numpy import zeros, ones, arange


def visualise_connectivity(S):

    Ns = len(S.source)

    Nt = len(S.target)

    plt.figure(figsize=(10, 4))
    prefs.codegen.target = "numpy"
    plt.subplot(121)

    plt.plot(zeros(Ns), arange(Ns), 'ok', ms=10)
    plt.plot(ones(Nt), arange(Nt), 'ok', ms=10)

    for i, j in zip(S.i, S.j):
        plt.plot([0, 1], [i, j], '-k')
    plt.xticks([0, 1], ['Source', 'Target'])
    plt.ylabel('Neuron index')

    plt.xlim(-0.1, 1.1)
    plt.ylim(-1, max(Ns, Nt))
    plt.subplot(122)
    plt.plot(S.i, S.j, 'ok')
    plt.xlim(-1, Ns)
    plt.ylim(-1, Nt)

    plt.xlabel('Source neuron index')
    plt.ylabel('Target neuron index')


tau_membrane = 20 * ms
tau_inhibitatory = 10 * ms
tau_excitatory = 5 * ms
membrane_resistence = 1e7 * ohm
v_init = -50 * mV
v_reset = -70 * mV
v_threshold = -50 * mV
v_rest = -70 * mV


NUM_EXCITATORY = 80
NUM_INHIBITATORY = 20
TOTAL_N = NUM_INHIBITATORY + NUM_EXCITATORY


CONN_E2E = 0.05
CONN_I2I = 0.0
CONN_E2I = 0.2
CONN_I2E = 0.8

SYNAPTIC_STRENGTH_E2E = 1e-7 * volt
SYNAPTIC_STRENGTH_E2I = 1e-8 * volt
SYNAPTIC_STRENGTH_I2I = -1e-8 * volt
SYNAPTIC_STRENGTH_I2E = -1e-8 * volt

# dv/dt = (ge-gi-(v-v_rest + 20))/tau_membrane : volt

eqs = """
dv/dt = (-v + v_rest + 20*volt)/tau_membrane : volt
"""

G = NeuronGroup(TOTAL_N, model=eqs, threshold="v > v_threshold", reset="v = v_reset", refractory=10*ms)
G.v = v_init


G_excitatory = G[:NUM_EXCITATORY]
G_inhibitatory = G[NUM_INHIBITATORY:]

SYN_E2E = Synapses(G_excitatory, G_excitatory, 'w: volt', on_pre='v_post += w')
SYN_E2E.connect(p=CONN_E2E)
SYN_E2E.w = SYNAPTIC_STRENGTH_E2E

SYN_E2I = Synapses(G_excitatory, G_inhibitatory, 'w: volt', on_pre='v_post += w')
SYN_E2I.connect(p=CONN_E2I)
SYN_E2I.w = SYNAPTIC_STRENGTH_E2I

SYN_I2E = Synapses(G_inhibitatory, G_excitatory, 'w: volt', on_pre='v_post += w')
SYN_I2E.connect(p=CONN_I2E)
SYN_I2E.w = SYNAPTIC_STRENGTH_I2E

SYN_I2I = Synapses(G_inhibitatory, G_inhibitatory, 'w: volt', on_pre='v_post += w')
SYN_I2I.connect(p=CONN_I2I)
SYN_I2I.w = SYNAPTIC_STRENGTH_I2I

G.v = ((v_threshold - v_reset)*rand(len(G))) + v_reset
# G.gi = ((v_threshold - v_reset)*rand(len(G))) + v_reset
# G.ge = ((v_threshold - v_reset)*rand(len(G))) + v_reset
# print(G.gi)

#
statemon = StateMonitor(G, 'v', record=True)
spikemon = SpikeMonitor(G, 'v', record=True)
run(0.1 * second)
plt.plot(statemon.t/ms, statemon.v[0], '-b', label='Neuron 0')
plt.show()
exit()

plt.plot(spikemon.t/ms, spikemon.i, '.k')
plt.xlabel('Time (ms)')
plt.ylabel('Neuron index')
plt.show()

# plt.raster
# plt.plot(M_E2E[0]['w'])
# plt.show()

# spikes = SpikeMonitor(lif)
# v_trace = StateMonitor(lif, "v", record=True)
# run(5 * second)
# plt.plot(v_trace.t / ms, v_trace.v.T / mV)
# plt.show()
