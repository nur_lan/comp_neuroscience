BACKPROP_MAX_SAMPLES = 4000
WEIGHTS_FILENAME = 'brain.npy'
MODEL_FILE_PREFIX = 'models'


# datasets type
DATA_TYPE = 'MNIST'

# paths
PATH2DATA = '../Data/'
PATH2RESULTS = '../Results/'
PATH2FIGURES = '../Figures/'

# algorithm selection
METHOD = 'BP'

# misc.
LOAD_WEIGHTS_FROM_FILE = False
SAVE_WEIGHTS_TO_FILE = False

# algorithm params
params_pla = {
    'eta': 0.5,
    'max_epoch': 1e6,
    'seed': 1,
    'standardize': False
}

params_bp = {
    'eta': 0.5,
    'eta_decay': 0.9,
    'hidden_units': 10,
    'output_units': 1,
    'tolerance': 0.1
}

params_hopfield = {
    'num_of_units': 550,
    'threshold': 0,
    'epochs': 1000,
    'num_memories': 10,
    'perturbation': 0.1,
    'tolerance': 0.05
}
