import numpy as np
from random import random, choice
import matplotlib.pyplot as plt
from scipy.stats import binom
from scipy.io import loadmat
from scipy.sparse.csgraph import floyd_warshall
from scipy.linalg import toeplitz


def plot_matrix(m, title="Weights"):
    # for i, w in enumerate(m):
    # if w<0:
    #     m[i] = 0
    plt.imshow(m, interpolation='nearest',
               cmap='binary', vmax=1, vmin=0)
    plt.title(title)
    plt.xticks(())
    plt.yticks(())
    plt.show()


def create_random_network(n, p):
    adjmat = np.zeros((n, n))
    for y in range(n):
        for x in range(y):
            adjmat[y, x] = 1 if random() < p else 0
            adjmat[x, y] = adjmat[y, x]  # symmetricity (non-directed graph)

    return adjmat


def average_rank(adj):
    adj_sum = np.sum(adj, axis=0)
    # print(adj_sum)
    return np.average(adj_sum)


def is_symmetric(M):
    for i in range(M.shape[0]):
        for j in range(M.shape[1]):
            if M[i, j] != M[i, j]:
                return False
    return True


def choose(n, k):
    """
    A fast way to calculate binomial coefficients by Andrew Dalke (contrib).
    """
    # n = int(n)
    k = int(k)
    if 0 <= k <= n:
        ntok = 1
        ktok = 1
        for t in range(1, min(k, n - k) + 1):
            ntok *= n
            ktok *= t
            n -= 1
        return ntok // ktok
    else:
        return 0


def mpl_from_shortest_paths(paths_M):
    n = paths_M.shape[0]
    sum = 0
    for i in range(n):
        for j in range(n):
            sum += paths_M[i, j]

    return sum / (n * (n - 1))


def local_clustering_coefficient(adj_M):
    n = adj_M.shape[0]
    neighbours_per_node = [[j for j in range(n) if adj_M[i, j] > 0] for i in range(n)]

    local_coefs = []
    for i in range(n):
        node_neighbors = neighbours_per_node[i]
        k_i = len(neighbours_per_node)
        neighbourhood_adj_mat = adj_M[node_neighbors, :][:, node_neighbors]
        e_i = np.sum(neighbourhood_adj_mat) / 2
        c_i = (2 * e_i) / (k_i * (k_i - 1))
        local_coefs.append(c_i)

    # print("Local coefs: {}. Length: {}".format(local_coefs, len(local_coefs)))
    return np.average(local_coefs)


def regular_lattice(k_half):
    first_row = np.zeros(n)
    for i in range(1, k_half):
        first_row[i] = 1
    for i in range(n - 1, n - k_half + 1, -1):
        first_row[i] = 1
    print(first_row)

    mat = toeplitz(first_row)
    print(mat)
    return mat


def degree_dist_for_net(adj_mat):
    print(adj_mat)
    n_ = adj_mat.shape[0]
    adj_sum = np.sum(adj_mat, axis=0)
    adj_sum = adj_sum[0]
    degree_counts = np.zeros(n_)
    for node in range(n_):
        degree_counts[adj_sum[0, node]] += 1.0

    return degree_counts


def q1():
    net = create_random_network(n, p)
    print("\nRandom network for n = " + str(n) + ", p = " + str(p) + ":\n")
    print(net)
    avg_rank = average_rank(net)
    avg_rank = round(100 * avg_rank) / 100
    print("\nAverage rank: " + str(round(100 * avg_rank) / 100))

    plot_matrix(net, title="Adjecency matrix for G(N={}, p={}). <k> = {}".format(n, p, avg_rank))


def q2():
    net = create_random_network(n, p)

    adj_sum = np.sum(net, axis=0)
    print(net)
    print(adj_sum)
    avg_rank = average_rank(net)
    print("average rank:", avg_rank)

    degree_counts = np.zeros(n)
    for node in range(n):
        degree_counts[adj_sum[node]] += 1.0

    degree_counts = degree_counts / n
    print(degree_counts)

    log_base = 2

    x = range(n)
    plt.xticks(x)
    plt.yticks(x)
    plt.legend(loc='best', frameon=False)
    plt.loglog(x, degree_counts, basex=log_base, basey=log_base, label='empirical pdf')

    plt.loglog(x, binom.pmf(x, n - 1, p), basex=log_base, basey=log_base, label='binom pdf')

    plt.title("PDF for binomial theoretical degree distribution and empirical for N={} p={}".format(n, p))
    plt.show()
    plt.close()

    plot_matrix(net, title="Adjecency matrix for G(N={}, p={}). <k> = {}".format(n, p, avg_rank))


def degrees_non_mat(net):
    n_ = net.shape[0]
    adj_sum = np.sum(net, axis=0)

    random_degree_counts = np.zeros(n_)
    for node in range(n_):
        random_degree_counts[adj_sum[node]] += 1.0

    random_degrees = random_degree_counts / n_
    return random_degrees


def q3_4_5():
    matlab_dict = loadmat('../Data/Coactivation_matrix.mat')
    brain_co_mat = np.matrix(matlab_dict['Coactivation_matrix'])
    print(brain_co_mat.shape)

    # Check symmetricality + Roof to 1
    for i in range(brain_co_mat.shape[0]):
        for j in range(brain_co_mat.shape[1]):
            if brain_co_mat[i, j] != brain_co_mat[i, j]:
                print("not symmetrical")
                break
            if brain_co_mat[i, j] > 0.05:
                brain_co_mat[i, j] = 1.0
            else:
                brain_co_mat[i, j] = 0.0

    # plot_matrix(brain_co_mat, "Brain co-activation adjacency matrix")


    ### Regular lattice matrix

    regular_mat = regular_lattice(200)
    average_k = average_rank(regular_mat)
    print("Regular lattice average rank: ", average_k)

    shortest_paths_matrix = floyd_warshall(regular_mat, directed=False)
    mpl = mpl_from_shortest_paths(shortest_paths_matrix)
    print("Regular lattice MPL: ", mpl)

    global_cf = local_clustering_coefficient(regular_mat)
    print("Regular lattice MCC:", global_cf)

    #### Brain Matrix

    shortest_paths_matrix = floyd_warshall(brain_co_mat, directed=False)
    average_k = average_rank(brain_co_mat)
    print("Brain Average rank: ", average_k)

    mpl = mpl_from_shortest_paths(shortest_paths_matrix)
    print("Brain MPL: ", mpl)

    global_cf = local_clustering_coefficient(brain_co_mat)
    print("Brain MCC:", global_cf)

    #### Random net matrix

    random_net = create_random_network(638, 0.5)

    average_k = average_rank(random_net)
    print("Random Average rank: ", average_k)

    random_shortest_paths_matrix = floyd_warshall(random_net, directed=False)
    mpl = mpl_from_shortest_paths(random_shortest_paths_matrix)
    print("Random MPL: ", mpl)

    global_cf = local_clustering_coefficient(random_net)
    print("Random MCC:", global_cf)

    brain_degrees = degree_dist_for_net(brain_co_mat)
    regular_degrees = degrees_non_mat(regular_mat)
    random_degrees = degrees_non_mat(random_net)

    log_base = 2
    x = range(n)
    xticks =  range(0,n+100,50)
    # plt.xticks(x)
    # plt.yticks(x)

    # plt.loglog(x, brain_degrees, basex=log_base, basey=log_base, label='Brain')
    # plt.loglog(x, random_degrees, basex=log_base, basey=log_base, label='Random')
    # plt.loglog(x, regular_degrees, basex=log_base, basey=log_base, label='Regular')

    plt.plot(x, np.log2(brain_degrees), label='Brain co-activation')
    plt.plot(x, np.log2(random_degrees), label='Random')
    plt.plot(x, np.log2(regular_degrees), label='Regular')
    plt.title('log(degree PDF) for each network')
    plt.legend(loc='best', frameon=False)
    plt.show()


def q4():
    matlab_dict = loadmat('../Data/Coactivation_matrix.mat')
    brain_co_mat = np.matrix(matlab_dict['Coactivation_matrix'])
    print(brain_co_mat.shape)

    # Check symmetricality + Roof to 1
    for i in range(brain_co_mat.shape[0]):
        for j in range(brain_co_mat.shape[1]):
            if brain_co_mat[i, j] != brain_co_mat[i, j]:
                print("not symmetrical")
                break
            if brain_co_mat[i, j] > 0.05:
                brain_co_mat[i, j] = 1.0
            else:
                brain_co_mat[i, j] = 0.0

    # plot_matrix(brain_co_mat, "Brain co-activation adjacency matrix")


    ### Regular lattice matrix

    regular_mat = regular_lattice(200)
    average_k = average_rank(regular_mat)
    print("Regular lattice average rank: ", average_k)

    shortest_paths_matrix = floyd_warshall(regular_mat, directed=False)
    mpl = mpl_from_shortest_paths(shortest_paths_matrix)
    print("Regular lattice MPL: ", mpl)

    global_cf = local_clustering_coefficient(regular_mat)
    print("Regular lattice MCC:", global_cf)

    #### Brain Matrix

    shortest_paths_matrix = floyd_warshall(brain_co_mat, directed=False)
    average_k = average_rank(brain_co_mat)
    print("Brain Average rank: ", average_k)

    mpl = mpl_from_shortest_paths(shortest_paths_matrix)
    print("Brain MPL: ", mpl)

    global_cf = local_clustering_coefficient(brain_co_mat)
    print("Brain MCC:", global_cf)

    #### Random net matrix

    random_net = create_random_network(638, 0.5)

    average_k = average_rank(random_net)
    print("Random Average rank: ", average_k)

    random_shortest_paths_matrix = floyd_warshall(random_net, directed=False)
    mpl = mpl_from_shortest_paths(random_shortest_paths_matrix)
    print("Random MPL: ", mpl)

    global_cf = local_clustering_coefficient(random_net)
    print("Random MCC:", global_cf)

    brain_degrees = degree_dist_for_net(brain_co_mat)
    regular_degrees = degrees_non_mat(regular_mat)
    random_degrees = degrees_non_mat(random_net)

    log_base = 2
    x = range(n)
    xticks =  range(0,n+100,50)
    # plt.xticks(x)
    # plt.yticks(x)

    # plt.loglog(x, brain_degrees, basex=log_base, basey=log_base, label='Brain')
    # plt.loglog(x, random_degrees, basex=log_base, basey=log_base, label='Random')
    # plt.loglog(x, regular_degrees, basex=log_base, basey=log_base, label='Regular')

    plt.plot(x, np.log2(brain_degrees), label='Brain co-activation')
    plt.plot(x, np.log2(random_degrees), label='Random')
    plt.plot(x, np.log2(regular_degrees), label='Regular')
    plt.title('log(degree PDF) for each network')
    plt.legend(loc='best', frameon=False)
    plt.show()


n = 638
p = 0.5

q3_4_5()
