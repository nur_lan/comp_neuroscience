from datasets import datasets
import numpy as np
import matplotlib.pyplot as plt

NUM_OF_SAMPLES_PER_CLUSTER = 10
NUM_OF_CLUSTERS = 3

TANH_ALPHA = 1
X_DIMENSION = 2
Y_DIMENSION = NUM_OF_CLUSTERS
ETA = 0.8
EPOCHS = 1000


def activation_func(input_vect):
    out_vect = []
    for y_i in input_vect:
        out_vect.append(np.tanh(TANH_ALPHA * y_i))
    return out_vect


def competition_func(y_vector):
    # winner takes all
    max_index = 0
    max = 0
    for index, y in enumerate(y_vector):
        if y > max:
            max = y
            max_index = index
    return_vector = [0] * len(y_vector)
    return_vector[max_index] = 1

    return return_vector


# generate samples
samples = datasets.generate_competetive_samples(X_DIMENSION, NUM_OF_CLUSTERS, NUM_OF_SAMPLES_PER_CLUSTER)
samples_x, samples_y = zip(*samples)

plt.scatter(samples_x, samples_y)
plt.axis([-1, 1, -1, 1])
plt.show()

# Init net
np.random.seed(100)
weights = np.matrix(
    [[np.random.ranf() for i in range(X_DIMENSION)] for row in range(Y_DIMENSION)]
)
print(weights)
y = np.zeros(Y_DIMENSION)



for epoch in range(EPOCHS):

    if epoch % 100 == 0:
        print("Epoch", epoch)
        print("Weights:", weights)

    for s, sample in enumerate(samples):
        sample_vector = np.array(sample)
        y_in_vector = (weights * sample_vector.reshape(X_DIMENSION,1))
        y_out = activation_func(y_in_vector)
        y_out_after_competition = competition_func(y_out)
        # print(y_out_after_competition)

        # update weights
        for i in range(weights.shape[0]):
            for j in range(weights.shape[1]):
                w_i_j_delta = sample[j] * y_out_after_competition[i]
                weights[i, j] = weights[i, j] + (ETA * w_i_j_delta)
                # print(w_i_j_delta)

        # standardize weights
        for row in range(weights.shape[0]):
            row_sqrt_sum_of_squares = np.sqrt(sum(list(map(lambda n: n**2, weights[row].tolist()[0]))))
            for col in range(weights.shape[1]):
                weights[row,col] = (weights[row,col] / row_sqrt_sum_of_squares)



# for row in range(weights.shape[0]):
#     plt.plot([0]*weights.shape[1], weights[row].tolist()[0])

plt.plot(weights[:,0], weights[:,1], 'ro', color='k')
plt.show()

print("Weights after training:", weights)


