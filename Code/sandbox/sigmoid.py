import matplotlib.pyplot as plt
from utils.utils import sigmoid
import numpy as np
from math import e


if __name__ == '__main__':

    for beta in range(1,5):

        x = np.array(np.arange(-5,5,0.1))
        y = np.array(list(map(lambda m: sigmoid(m, beta), x)))

        plt.plot(x,y)

    plot_margin = 0.1
    x0, x1, y0, y1 = plt.axis()
    plt.axis((x0 - plot_margin,
              x1 + plot_margin,
              y0 - plot_margin,
              y1 + plot_margin))

    plt.xlabel('a')
    plt.ylabel('sigmoid(a)')
    plt.show()
