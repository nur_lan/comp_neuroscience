/*
 * This is a JavaScript Scratchpad.
 *
 * Enter some JavaScript, then Right Click or choose from the Execute Menu:
 * 1. Run to evaluate the selected text (Ctrl+R),
 * 2. Inspect to bring up an Object Inspector on the result (Ctrl+I), or,
 * 3. Display to insert the result in a comment after the selection. (Ctrl+L)
 */


var num_cells = 25;
var cells = new Array(num_cells);
var wgt = new Array(num_cells);


////////// helpers


function rnd(min, max){ return Math.floor((max-min)*Math.random())+min; }

function resetWeights(){
  for(var r = 0; r<num_cells; r++){
    wgt[r] = new Array(num_cells);
    for(var c = 0; c<num_cells; c++){
      wgt[r][c] = 0;
    }
  }
}


function display(data, width){
  var res = "";
  for(var i = 0; i<data.length; i++){
    res += (data[i] == 1 ? "*" : ".") + " ";
    if((i+1) % width == 0) res += "\n";
  }
  console.log(res);
  return res;
}

//////// algorithm

function runNetwork(input){
  for(var i = 0; i<num_cells; i++){
    cells[i] = input[i];
  }
  threshold = 0;
  
  /// update cells
  var num_epochs = 200;
  
  function checkCell(i){
    var sum = 0;
    
    for(var j = 0; j<num_cells; j++){
      sum += wgt[j][i]*cells[j];
    }
    
    return (sum > threshold ? 1 : -1);
  }
  
  for(var i = 0; i<num_epochs; i++){
    c = rnd(0, num_cells);
    cells[c] = checkCell(c);
  }
  return cells;
}

function trainNetwork(patterns){
  //console.log("!!!", patterns);
  resetWeights();
  
  for(var r = 0; r<num_cells; r++){
    for(var c = 0; c<r; c++){
      var sum = 0;
      
      for(var p = 0; p<patterns.length; p++){
        sum += patterns[p][r] * patterns[p][c];
      }
      
      wgt[r][c] = sum / patterns.length;
      wgt[c][r] = wgt[r][c];
    }
  }
  console.log(wgt);
}

///////////  tests


var p0 = [[-1, -1, -1, -1, -1], [1, 1, 1, 1, 1], [-1, -1, 1, -1, -1], [-1, -1, 1, -1, -1], [-1, -1, 1, -1, -1]]; // T
var p1 = [[-1, -1, 1, -1, -1], [-1, 1, -1, 1, -1], [1, -1, -1, -1, 1], [-1, 1, -1, 1, -1], [-1, -1, 1, -1, -1]]; // O
var i0 = [[-1, -1, -1, -1, -1], [1, 1, 1, -1, -1], [-1, -1, 1, -1, -1], [-1, -1, 1, -1, -1], [-1, -1, -1, -1, -1]];

p0 = [-1, -1, -1, -1, -1,    1, 1, 1, 1, 1,       -1, -1, 1, -1, -1,     -1, -1, 1, -1, -1,     -1, -1, 1, -1, -1]; // T
p1 = [-1, -1, 1, -1, -1,    -1, 1, -1, 1, -1,     1, -1, -1, -1, 1,      -1, 1, -1, 1, -1,      -1, -1, 1, -1, -1]; // O
i0 = [-1, -1, -1, -1, -1,    1, 1, 1, -1, -1,     -1, -1, 1, -1, -1,     -1, -1, 1, -1, -1,     -1, -1, -1, -1, -1];
i1 = [-1, -1, 1, -1, -1,    1, -1, -1, -1, -1,     1, -1, -1, -1, 1,     -1, -1, -1, -1, -1,     -1, -1, 1, -1, -1];
i2 = [-1, -1, -1, -1, -1,   -1, -1, -1, -1, -1,   -1, -1, -1, -1, -1,    -1, -1, -1, -1, -1,    -1, -1, -1, -1, 1];
i3 = [1, 1, 1, 1, 1,        -1, -1, -1, -1, -1,   -1, -1, -1, -1, -1,    -1, -1, -1, 1, 1,    -1, -1, -1, 1, 1];

input = i3;

trainNetwork([p0, p1]);
display(input, 5);
display(runNetwork(input), 5);


/*
. . * . . 
. * . * . 
* . . . * 
. * . * . 
. . * . . 

*/
/*
. . . . . 
* * * * * 
. . * . . 
. . * . . 
. . * . . 


*/
/*
* * * * * 
. . . . . 
* * . * * 
* * . * * 
* * . * * 

*/