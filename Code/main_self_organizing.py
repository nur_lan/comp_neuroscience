from models.self_organizing_map import Self_Organizing_Map_Model
from models.model import Model

LOAD = True
FILE = './bin_dumps/self_organizing_circle_2.bin'

if LOAD:
    model = Model.load_model_from_file(FILE)
else:
    model = Self_Organizing_Map_Model(second_dataset=True)
    model.train_model()
    model.save_model_to_file(model, FILE)


model.plot_weights("Samples (blue) and weights(red) after 6000 epochs")