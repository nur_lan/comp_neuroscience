from random import random, shuffle
import numpy as np
from math import sqrt, log
import matplotlib.pyplot as plt
from multiprocessing import Pool
import pickle


###############
# hyper params
###############

LOAD_WEIGHTS_FROM_FILE = True
WEIGHTS_FILE = "./bin_dumps/som.bin"

NUM_SAMPLES = 300
NUM_SOM_UNITS = 20
LATTICE_COLUMNS = 5
LATTICE_ROWS = int(NUM_SOM_UNITS / LATTICE_COLUMNS)
INPUT_DIMENSION = 2

SO_MAX_EPOCH = 2000
CONVERGENCE_MAX_EPOCH = 500*20
TOLERANCE = 1e-4

SIGMA_0 = NUM_SOM_UNITS/2
TAU_SIGMA = 1000/log(SIGMA_0)
ETA_0 = 0.1
TAU_ETA = -1 * SO_MAX_EPOCH/log(0.001/ETA_0)

SIGMA = SIGMA_0
ETA = ETA_0


###############
# helpers
###############

def save_to_file(weights_matrix):
    f = open(WEIGHTS_FILE, mode='wb')
    pickle.dump(weights_matrix, file=f)
    print("Saved to " + WEIGHTS_FILE)


def load_from_file():
    w = pickle.load(open(WEIGHTS_FILE, mode='rb'))
    print("Loaded")
    return w


def lattice_distance(coord_1, coord2):
    dx = coord_1[0]-coord2[0]
    dy = coord_1[1]-coord2[1]
    return sqrt(dx**2 + dy**2)


def weights_distance(arr1, arr2):
    diff_arr = arr1 - arr2
    distance = np.linalg.norm(diff_arr)
    return distance


def gaussian(d, sigma):
    left = (sqrt(2 * sigma**2 * np.math.pi)) ** -1
    left = 1
    return left * np.math.e ** ((-1 * (d**2)) / (2 * (sigma**2)))


def coord_for_unit_index(index):
    row = int(index / LATTICE_ROWS)
    column = index % LATTICE_COLUMNS
    return row, column


def index_for_coord(coord_tuple):
    row = coord_tuple[0]
    column = coord_tuple[1]
    index = row * LATTICE_COLUMNS + column
    return index


def eta_at_time(t):
    return ETA_0 * np.math.e ** ((-1 * t) / TAU_ETA)


def sigma_at_time(t):
    return SIGMA_0 * np.math.e ** ((-1 * t) / TAU_SIGMA)



###############
# generate samples
###############

x_values = [(random()*2)-1 for x in range(int(NUM_SAMPLES/2))]
y_values = [sqrt(1-(x**2))+0.05*random() for x in x_values]

x_2 = [(random()*2)-1 for x in range(int(NUM_SAMPLES/2))]
y_2 = [-1 * sqrt(1-(x**2))+0.05*random() for x in x_2]

x_values.extend(x_2)
y_values.extend(y_2)

# plt.scatter(x_values, y_values)
# plt.title("Samples scatter")
# plt.xlabel('x')
# plt.ylabel('y')
# plt.show()

samples = []
for s in range(NUM_SAMPLES):
    sample = np.array([x_values[s], y_values[s]])
    samples.append(sample)

print("Samples:", samples)


def plot_weights(title):
    plt.scatter(x_values, y_values, color='blue')
    plt.title("Samples+weights scatter")
    # plt.xlabel('x')
    # plt.ylabel('y')
    # plt.show()

    plt.scatter(weights[:, 0], weights[:, 1], color='red')
    plt.title(title)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()

###############
# init weights
###############

weights = np.zeros((NUM_SOM_UNITS, INPUT_DIMENSION))
for x in range(NUM_SOM_UNITS):
    for y in range(INPUT_DIMENSION):
        weights[x,y] = random()
print("Initialized random weights:", weights)


###############
# init units
###############

lattice = []
for y in range(LATTICE_ROWS):
    lattice.append([])
    for x in range(LATTICE_COLUMNS):
        coord = (x,y)
        lattice[y].append(coord)
print("Lattice coordinates:", lattice)


###############
# main algorithm
###############


if LOAD_WEIGHTS_FROM_FILE:
    weights = load_from_file()
    plot_weights("Samples (blue) and weights(red)")


for epoch in range(20000):

    if epoch % 300 == 0:
        print("ETA:", ETA)
        print("SIGMA:", SIGMA)
        print(epoch)
        plot_weights("Samples (blue) and weights(red) after {} epochs".format(str(epoch)))

    sample_indeces = list(range(NUM_SAMPLES))
    shuffle(sample_indeces)
    for sample_index in sample_indeces:  #TODO CHOOSE RANDOMLY
        sample = samples[sample_index]

        min_distance = 2 ** 32
        best_matching_unit_index = 0
        for unit_index in range(NUM_SOM_UNITS):
            unit_weights = weights[unit_index]
            distance_to_input = weights_distance(sample, unit_weights)
            if distance_to_input < min_distance:
                min_distance = distance_to_input
                best_matching_unit_index = unit_index

        best_matching_unit_coord = coord_for_unit_index(best_matching_unit_index)

        # def poolf(unit_index):
        #     unit_weights = weights[unit_index]
        #     distance_from_bmu = lattice_distance((x, y), best_matching_unit_coord)
        #     unit_weights = unit_weights + ETA * gaussian(distance_from_bmu, SIGMA) * (sample - unit_weights)
        #     return unit_weights
        #
        # pool = Pool(3)
        # new_weights = pool.map(poolf, range(NUM_SOM_UNITS))
        # for w, weights_for_unit in enumerate(new_weights):
        #     weights[w] = weights_for_unit
        #

        for unit_index in range(NUM_SOM_UNITS):
        # for x, row in enumerate(lattice):
        #     for y, unit_tuple in enumerate(row):
        #         unit_index = index_for_coord((x, y))
            unit_weights = weights[unit_index]
            x, y = coord_for_unit_index(unit_index)
            distance_from_bmu = lattice_distance((x,y), best_matching_unit_coord)
            weights[unit_index] = unit_weights + ETA * gaussian(distance_from_bmu, SIGMA) * (sample - unit_weights)

    ETA = eta_at_time(epoch)
    SIGMA = sigma_at_time(epoch)

plot_weights("Final weights")