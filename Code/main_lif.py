import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt

TAUM_MEMBRANE_CHARACTERISTIC_TIME = 0.1  # s
THRESHOLD_VOLTAGE = -0.05  # mV
EL_REST_POTENTIAL = -0.07  # mV
RM_MEMBRANE_RESISTANCE = 1  # Ohm
V_INIT = -0.07

# ELECTRODE CURRENT
CURRENT_TYPE = 'sinusoidal'  # constant or 'sinusoidal'
IE_CURRENT_CONSTANT = 0.04  # Amper
ELECTRODE_TIME_VEC = np.arange(0, 5)

ELECTRODE_CURRENT = []
for t in ELECTRODE_TIME_VEC:
    if CURRENT_TYPE == 'constant':
        ELECTRODE_CURRENT.append(IE_CURRENT_CONSTANT)
    if CURRENT_TYPE == 'sinusoidal':
        ELECTRODE_CURRENT.append(np.sin(t))


def current(t):
    return np.sin(t % np.pi)  # + 0.001*(np.random.standard_normal()-0.5)


def Vsimple(t):
    ret = RM_MEMBRANE_RESISTANCE * IE_CURRENT_CONSTANT * \
          (1 - np.exp(-1 * (t/TAUM_MEMBRANE_CHARACTERISTIC_TIME)))


def V(t):
    print(t)
    ret = (EL_REST_POTENTIAL + (RM_MEMBRANE_RESISTANCE * IE_CURRENT_CONSTANT)) + \
          (
              (V_INIT - EL_REST_POTENTIAL - (RM_MEMBRANE_RESISTANCE * IE_CURRENT_CONSTANT)) * np.exp(
                  ((-1 * t) / TAUM_MEMBRANE_CHARACTERISTIC_TIME))
          )

    return ret


def dV(v, t=0):
    current = IE_CURRENT_CONSTANT
    res = (1 / TAUM_MEMBRANE_CHARACTERISTIC_TIME) * (
        EL_REST_POTENTIAL - v + (RM_MEMBRANE_RESISTANCE * current))
    if res >= THRESHOLD_VOLTAGE:
        return [EL_REST_POTENTIAL]
    return res


# Integrate dV

T = np.arange(0, 1, 0.1)
print(T)

V_integrated = integrate.odeint(dV, np.array([V_INIT]), T)


plt.plot(T, V_integrated)
plt.show()
