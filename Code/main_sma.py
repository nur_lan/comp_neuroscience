import scipy.io as sio
import numpy as np
from pandas import DataFrame
import matplotlib.pyplot as plt

matlab_dict = sio.loadmat('../Data/dataset_SMA.mat')
# print(matlab_dict)
X = matlab_dict['X']
T = matlab_dict['T']

NUM_TRIALS = len(T[0])
NUM_NEURONS = len(X)

# print('\n\nX:', X.__str__())
# print('\n\nT:', T.__str__())
print("X")
print(DataFrame(X))
print("T")
print(DataFrame(T))

#
# calculate the joint distribution between spike count and movement direction

max_spike = -1 *(2**32)
for neuron_index in X:
    max_in_row = max(neuron_index)
    if max_in_row > max_spike:
        max_spike = max_in_row
print(max_spike)
spike_values = list(range(max_spike + 1))
direction_vals = list(range(9))

neuron_index = 0
mutual_infos = [0] * NUM_NEURONS
for neuron_row in X:

    ## MEDIAN STRATEGY ##
    neuron_median = np.median(neuron_row)
    median_row = [1 if x>=neuron_median else 0 for x in neuron_row]
    spike_values = [0, 1]
    print("median:", neuron_median)
    print(median_row)
    print(neuron_row)
    histogram = np.zeros((len(spike_values), len(direction_vals)))
    joint_dist = np.zeros((len(spike_values), len(direction_vals)))

    for trial, spike_activation in enumerate(median_row):
        direction = T[0, trial]
        histogram[spike_activation, direction] += 1
        joint_dist[spike_activation, direction] = (histogram[spike_activation, direction] / NUM_TRIALS)
    # print("\nHistogram for neuron {}\n================================\n:".format(neuron_index + 1), DataFrame(histogram))
    # print("\nJoint Dist for neuron {}\n================================\n:".format(neuron_index + 1), DataFrame(joint_dist))

    # sanity check for joint distribution

    p_spikes = [np.sum(row) for row in joint_dist]
    total_p_spikes = np.sum(p_spikes)
    p_directions = [np.sum(col) for col in joint_dist.T]
    total_p_directions = np.sum(p_directions)

    if total_p_spikes - 1.0 > 0.000000001 or total_p_directions - 1.0 > 0.000000001:
        print("Sanity check error")
        break

    mutual_info = 0
    for spike_index in range(len(spike_values)):
        for direction_index in range(len(direction_vals)):
            p_x_y = joint_dist[spike_index, direction_index]
            p_x = p_spikes[spike_index]
            p_y = p_directions[direction_index]
            if p_x == 0 or p_y == 0 or p_x_y == 0:
                continue
            else:
                mutual_info += p_x_y * np.math.log2(p_x_y / (p_x * p_y))

    print("Mutual info for neuron {}: {}".format(neuron_index+1, mutual_info))
    mutual_infos[neuron_index] = mutual_info
    neuron_index += 1

print(",".join([str(x) for x in range(1,len(mutual_infos)+1)]))
print(",".join([str(x) for x in mutual_infos]))
print("Maximal mutual info neuron: {}".format(mutual_infos.index(max(mutual_infos))+1))

y_mutual_info_percentage = [mi / 3 for mi in mutual_infos]
x_neurons = list(range(1,NUM_NEURONS+1))

plt.plot(x_neurons, y_mutual_info_percentage)
plt.title("M.I per neuron relative to directionalty maximum information")
plt.xlabel("Neuron #")
plt.ylabel("mutual info / log(8)")
plt.xticks(x_neurons)
plt.show()
